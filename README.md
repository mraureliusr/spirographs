Spirographs!

![Screenshot of software](https://frzn.dev/~amr/images/spirographspy.png)

Everyone who grew up playing with this toy remembers it fondly. Very cool shapes and patterns. Recently, I was wondering if there was a mathematical formula describing the shapes created by Spriographs, and of course there is!

They're actually quite simple. The shapes are called [hypotrochoids](https://en.wikipedia.org/wiki/Hypotrochoid) and [epitrochoids](https://en.wikipedia.org/wiki/Epitrochoid). To calculate each point, you simple use the following parametrized equations, plugging in 0 to 2π for θ:

![](https://frzn.dev/~amr/images/hypotrochoid.png)

![](https://frzn.dev/~amr/images/epitrochoid.png)

And that's it! To play around with the different patterns, this Python GTK3 app was created so you could use sliders to change the parameters and see how they affect the output.
